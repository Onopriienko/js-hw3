function mathOperation(firstNumber, secondNumber, operation) {
    switch (operation) {
        case `+`:
            return firstNumber + secondNumber;
        case `*`:
            return firstNumber * secondNumber;
        case `-`:
            return firstNumber - secondNumber;
        case `/`:
            return firstNumber / secondNumber;
    }
}
let firstNumber = prompt("Please enter first number");
let secondNumber = prompt("Please enter second number");

    while (isNaN(firstNumber) || isNaN(secondNumber) || !firstNumber || !secondNumber){
        firstNumber = prompt("Please enter first number", firstNumber);
        secondNumber = prompt("Please enter second number", secondNumber);
    }

    firstNumber = +firstNumber;
    secondNumber = +secondNumber;

let operation = prompt("Please enter operation  (+, -, *, /)");
let result = mathOperation(firstNumber, secondNumber, operation);
console.log(result);